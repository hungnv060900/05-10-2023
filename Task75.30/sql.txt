Hiển thị toàn bộ thông tin của tất cả môn học:


SELECT * FROM subjects;
Hiển thị mã môn, tên môn, và số tín chỉ của 3 sinh viên:


SELECT subject_code, subject_name, credit FROM subjects LIMIT 3;
Hiển thị toàn bộ thông tin 4 môn có số tín chỉ thấp nhất:


SELECT * FROM subjects ORDER BY credit ASC LIMIT 4;
Hiển thị thông tin của sinh viên có mã sinh viên là: 20101234:

SELECT * FROM students WHERE student_code = '20101234';
Hiển thị toàn bộ danh sách điểm của môn MAT101:


SELECT s.subject_code, s.subject_name, st.user_name, g.grade, g.exam_date
FROM grades g
INNER JOIN students st ON g.student_id = st.id
INNER JOIN subjects s ON g.subject_id = s.id
WHERE s.subject_code = 'MAT101';
Lấy ra danh sách các môn có ngày thi là ngày 2021-05-04:


SELECT subject_code, subject_name, exam_date
FROM subjects s
INNER JOIN grades g ON s.id = g.subject_id
WHERE DATE(g.exam_date) = '2021-05-04';
Lấy ra thông tin 3 điểm thi thấp nhất: môn học, tên sinh viên, điểm thi, ngày thi:


SELECT s.subject_name, st.user_name, g.grade, g.exam_date
FROM grades g
INNER JOIN students st ON g.student_id = st.id
INNER JOIN subjects s ON g.subject_id = s.id
ORDER BY g.grade ASC
LIMIT 3;
Lấy ra thông tin 1 sinh viên có điểm cao nhất của môn ECO101:


SELECT s.subject_name, st.user_name, MAX(g.grade) AS max_grade
FROM grades g
INNER JOIN students st ON g.student_id = st.id
INNER JOIN subjects s ON g.subject_id = s.id
WHERE s.subject_code = 'ECO101'
GROUP BY s.subject_name, st.user_name
ORDER BY max_grade DESC
LIMIT 1;