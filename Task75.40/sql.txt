Hiển thị toàn bộ thông tin khách hàng (customers) có credit > 50000:

sql
Copy code
SELECT * FROM customers WHERE credit > 50000;
Hiển thị toàn bộ thông tin danh sách sản phẩm với tên của product line tương ứng:

sql
Copy code
SELECT p.product_name, pl.product_line_name
FROM products p
INNER JOIN product_lines pl ON p.product_line_id = pl.product_line_id;
Đếm xem ở từng nước có bao nhiêu khách hàng:

sql
Copy code
SELECT country, COUNT(*) AS total_customers
FROM customers
GROUP BY country;
Sắp xếp các sản phẩm theo giá mua tăng dần và lấy ra 10 sản phẩm đầu tiên:

sql
Copy code
SELECT * FROM products
ORDER BY buy_price ASC
LIMIT 10;
Lấy ra danh sách các đơn hàng được đặt vào tháng 10/2003:

sql
Copy code
SELECT * FROM orders
WHERE MONTH(order_date) = 10 AND YEAR(order_date) = 2003;
Lấy ra danh sách các khách hàng có số lượng đơn hàng > 3:

sql
Copy code
SELECT c.customer_name, COUNT(o.order_id) AS order_count
FROM customers c
LEFT JOIN orders o ON c.customer_id = o.customer_id
GROUP BY c.customer_name
HAVING order_count > 3;
Tính toán xem, mỗi đơn hàng có bao nhiêu sản phẩm được đặt và tổng tiền từng đơn hàng là bao nhiêu:

sql
Copy code
SELECT o.order_id, COUNT(od.product_id) AS total_products, SUM(od.quantity_ordered * od.price_each) AS total_price
FROM orders o
INNER JOIN order_details od ON o.order_id = od.order_id
GROUP BY o.order_id;
Tính toán xem, mỗi đơn hàng có bao nhiêu sản phẩm được đặt và tổng tiền từng đơn hàng là bao nhiêu, và lấy ra danh sách các đơn có tổng tiền > 100000:

sql
Copy code
SELECT o.order_id, COUNT(od.product_id) AS total_products, SUM(od.quantity_ordered * od.price_each) AS total_price
FROM orders o
INNER JOIN order_details od ON o.order_id = od.order_id
GROUP BY o.order_id
HAVING total_price > 100000;
Tìm và hiển thị toàn bộ thông tin sản phẩm được đặt nhiều nhất (theo quantity_ordered) trong năm 2004:

sql
Copy code
SELECT p.product_name, SUM(od.quantity_ordered) AS total_quantity_ordered
FROM order_details od
INNER JOIN products p ON od.product_id = p.product_id
INNER JOIN orders o ON od.order_id = o.order_id
WHERE YEAR(o.order_date) = 2004
GROUP BY p.product_name
ORDER BY total_quantity_ordered DESC
LIMIT 1;